# eCMS POC
Sample proof of concept for the eCMS application. Swagger + Spring Cloud Netflix + Zipkin + Angular + Bootstrap. 

Description
=========

The project depends on 6 modules in Docker containers deployed via Docker-Compose: 

- **eureka-server:** a Eureka server (Spring Boot application) for service discovery and registration. The Eureka dashboard is available at http://localhost:8761/ after you've built and deployed the server. 
- **db-maria:** a Maria DB database 
- **ecms-airlines-service:** an Airlines microservice (Spring Boot application) that handles CRUD actions on airline objects. The API was defined and generated with Swagger Editor. The Swagger UI, allowing you to test the API, is available at http://localhost:81/ after you've built and deployed the project. It is a Eureka client and registers itself as such. It also leverages Hystrix so that if the database fails on the getAirlines method, a fallback method will be called and return an empty array of airlines. 
- **ecms-ui**: A very simple UI (Sprint Boot application) written with Angular, Bootstrap and launched via Spring Boot. It is a Eureka client and registers itself as such. The UI on its own is available at http://localhost:82/ui/ but it cannot route to the airlines service without the Gateway.  
- **ecms-gateway**: A Zuul Proxy Server as the API Gateway (Spring Boot application) leveraging Eureka, Ribbon and Hystrix to route calls from http://localhost:80/ to the airlines/ and ui/ endpoints. 
- **zipkin-server**: A Zipkin server that will collect the traces sent by Spring Cloud Sleuth-enabled applications (UI, Gateway, and Airlines service)
- **config-server**: A Spring Cloud config server to handle the configuration of the applications. It includes Git already installed and set up and shares a volume with the local framework. For now only the config of ecms-airlines is online, the other applications contain their own configuration.
- **rmq**: A Rabbit MQ server to allow services to communicate with each other via a RabbitMQ bus. 


> **Note**: This was tested on Mac OS X (El Capitan). Docker Compose leverages mounted volumes, so it might cause issues on Windows. You might want to prefer connecting to an external database rather than using mounted data volumes for the mariadb instance. 



How to install
=========

1. Install Docker and Docker-Compose 
2. Clone the repository 
3. Build the Airlines Service Maven project: 

    ```shell
    cd <project_root>/ecms-airlines-service/
    mvn clean package 
    ```
4. Build the API Gateway (Zuul Proxy) Maven project: 

    ```shell
    cd <project_root>/ecms-gateway/
    mvn clean package 
    ```
5. Build the UI Maven project: 

    ```shell
    cd <project_root>/ecms-ui/
    mvn clean package 
    ```
6. Build the Eureka Server Maven project:  

    ```shell
    cd <project_root>/eureka-server/
    mvn clean package 
    ``` 
7. Build the Spring Cloud Config project:  

    ```shell
    cd <project_root>/config-server/
    mvn clean package 
    ``` 
8. Go to the root and run docker-compose: 
    ```shell
    cd <project_root>/
    docker-compose build
    docker-compose up -d 
    ```
    > **Note**: You can also use the build_and_launch.sh script at the root of the repository to build the Maven projects and launch Docker Compose commands at the same time. It doesn't rebuild the Eureka server; you will still have to build it manually. 
    
9. Check all 8 containers are deployed:
    ```shell
    docker ps
    ```
    > **Note**: You can also open Kitematic to check the status and logs of each container. 
    
10. Check the logs of the springcloudpoc_db-maria_1 container to find the GENERATED ROOT PASSWORD.
11. Use the GENERATED ROOT PASSWORD one time password to change the root password and run the setup script (which you will copy from the repo into your container in the first line):
 
    ```shell
    docker cp <REPO_ROOT>/ecms-mariadb/setup/test-setup-airlines-db.sql springcloudpoc_db-maria_1:/tmp
    docker exec -it springcloudpoc_db-maria_1 bash
    mysqladmin -u root -p'GENERATED_ROOT_PASSWORD' password 'YOUR_SECRET_ROOT_PASSWORD' 
    mysql -u root -p'YOUR_SECRET_ROOT_PASSWORD' < /tmp/test-setup-airlines-db.sql
    ```
12. Configure the Git server of the Config Server:
 
    ```shell 
    docker exec -it springcloudpoc_config-server_1 sh
    cd /repo
    git init .
    git config --global user.name "YOUR_USERNAME"
    git config --global user.email "YOUR_EMAIL"
    git add .
    git commit -m "Initializing configuration"
    ```

13. Open the Eureka Server GUI at http://localhost:8761. You should see the Eureka GUI and within the registered instances, one registered ECMS-AIRLINES instance, one ECMS-GATEWAY instance, one CONFIG-SERVER instance, and one ECMS-UI instance. It might take a while; wait for all instances to be redirected. If the ECMS-AIRLINES instance complains that it cannot reach the configuration server, wait for the CONFIG-SERVER to be available, then restart the ECMS-AIRLINES instance.
14. Check out the GUI through the gateway at http://localhost/ui/ (Don't forget the trailing slash)
15. You should see "welcome to eCMS!" as well as the number of airlines in the database returned by the Airlines service. You can add some using the Swagger UI of your API airlines service at http://localhost:81/ (use the POST airlines/ operation)
16. At http://localhost/airlines, you should see the JSON answer of your Airlines service. You should see your new airline's info.
17. Refresh http://localhost/ui/ to see your newly added airline in the GUI.
18. Analyze the traces logged into Zipkin at http://localhost:9411 919. Try scaling! 
    ```shell 
    docker-compose scale airlines-service=3
    docker ps
    ```
   Now you shoud see three instances of the ECMS-AIRLINES-SERVICE running and registered in Eureka! Stop any of them and see if your http://localhost/ui/ or http://localhost/airlines still works :)

20. Done!! 
