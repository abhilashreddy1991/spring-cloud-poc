#!/bin/bash

cd ./ecms-gateway/
mvn package
cd ../
cd ./ecms-airlines-service/
mvn package
cd ../
cd ./ecms-ui/
mvn package
cd ../
cd ./config-server/
mvn package
cd ../
cd ./eureka-server/
mvn package
docker-compose build 
docker-compose up -d --remove-orphans 
