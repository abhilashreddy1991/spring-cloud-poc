package org.thales;
  
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController 
@EnableEurekaClient
@RefreshScope
public class EcmsWebApplication {


    @Value("${welcomeMessage:Welcome to eCMS default}")
    private String welcomeMessage;
 
    @RequestMapping(value = "/welcomeMessage",
            produces = { "text/plain" }  ) 
    String getWelcomeMessage() {
        return this.welcomeMessage;
    }

	public static void main(String[] args) {
		SpringApplication.run(EcmsWebApplication.class, args);
	}
	  
}
