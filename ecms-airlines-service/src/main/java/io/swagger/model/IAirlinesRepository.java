package io.swagger.model;

import javax.transaction.Transactional;
import java.util.List; 
import org.springframework.data.repository.CrudRepository;
import io.swagger.model.Airline; 
 
@Transactional
public interface IAirlinesRepository extends CrudRepository<Airline, Long> {

	  public List<Airline> findByName(String name);
	  public List<Airline> findByCountry(String country);
	  public List<Airline> findByIcaoCode(String icaoCode);
	  public List<Airline> findByActive(Boolean active);
	  public List<Airline> findById(Long id);
}  
 
  