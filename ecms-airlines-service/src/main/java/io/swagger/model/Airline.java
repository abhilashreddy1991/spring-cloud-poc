package io.swagger.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.slf4j.LoggerFactory;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.api.AirlinesApiController;




/**
 * Airline
 */
@Entity
@Table(name="airlines")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-28T23:18:49.392Z")

public class Airline   {

  private static Logger logger = LoggerFactory.getLogger(Airline.class);
	
  @Id	
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id = null;

  @NotNull
  private String name = null;

  @NotNull
  private String icaoCode = null;

  @NotNull
  private String country = null;
 
  private String website = null;

  @NotNull
  @Type(type = "org.hibernate.type.NumericBooleanType")
  private Boolean active = null;

  public Airline() {
      
  }
 
  
  public Airline id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * ID of the airline in the system.
   * @return id
  **/
  @ApiModelProperty(value = "ID of the airline in the system.")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Airline name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The airline's full name.
   * @return name
  **/
  @ApiModelProperty(required = true, value = "The airline's full name.")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Airline icaoCode(String icaoCode) {
    this.icaoCode = icaoCode;
    return this;
  }

   /**
   * ICAO code of the airline.
   * @return icaoCode
  **/
  @ApiModelProperty(required = true, value = "ICAO code of the airline.")
  public String getIcaoCode() {
    return icaoCode;
  }

  public void setIcaoCode(String icaoCode) {
    this.icaoCode = icaoCode;
  }

  public Airline country(String country) {
    this.country = country;
    return this;
  }

   /**
   * Home country of the airline
   * @return country
  **/
  @ApiModelProperty(required = true, value = "Home country of the airline")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Airline website(String website) {
    this.website = website;
    return this;
  }

   /**
   * Website of the airline.
   * @return website
  **/
  @ApiModelProperty(required = true, value = "Website of the airline.")
  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public Airline active(Boolean active) {
    this.active = active;
    return this;
  }

   /**
   * Status of the airline in the system (active/inactive)
   * @return active
  **/
  @ApiModelProperty(required = true, value = "Status of the airline in the system (active/inactive)")
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) { 
    this.active = active;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Airline airline = (Airline) o;
    return Objects.equals(this.id, airline.id) &&
        Objects.equals(this.name, airline.name) &&
        Objects.equals(this.icaoCode, airline.icaoCode) &&
        Objects.equals(this.country, airline.country) &&
        Objects.equals(this.website, airline.website) &&
        Objects.equals(this.active, airline.active);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, icaoCode, country, website, active);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Airline {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    icaoCode: ").append(toIndentedString(icaoCode)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    website: ").append(toIndentedString(website)).append("\n");
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

