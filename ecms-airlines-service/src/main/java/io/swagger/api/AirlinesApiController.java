package io.swagger.api;

import io.swagger.model.Airline;  
import io.swagger.model.IAirlinesRepository;
import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestParam;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import java.util.ArrayList;
import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-28T23:18:49.392Z")

@Controller  
public class AirlinesApiController implements AirlinesApi {

	/***************************************************************
	 * MEMBER DATA 
	 ***************************************************************/
	@Autowired
    private IAirlinesRepository airlinesRepo;

	private static Logger logger = LoggerFactory.getLogger(AirlinesApiController.class);
	
	
	/***************************************************************
	 * API FUNCTIONS
	 ***************************************************************/
	
	
    public ResponseEntity<Airline> createAirline(

@ApiParam(value = "New airline to add to the portal" ,required=true ) @RequestBody Airline airline

) {
		// Initialization 
		HttpStatus status = HttpStatus.NO_CONTENT; 
		
		// Try to create the new airline 
    	try 
    	{ 
    		airline = airlinesRepo.save(airline);  
    		status = HttpStatus.CREATED;
    		return new ResponseEntity<Airline>(airline, status);
	    }
	    catch (Exception ex) 
    	{ 
	    	logger.error("Error creating the airline: " + ex.toString());
	    	status = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<Airline>(airline, status);
	    } 
		 
    }

    @HystrixCommand(fallbackMethod = "fallbackGetAirlines")
    public ResponseEntity<List<Airline>> getAirlines(
    	@ApiParam(value = "Filter on the airline ID ( equivalent to GET /airline/{id} )") 	@RequestParam(value = "id", required = false) Long id,
        @ApiParam(value = "Filter on the airline's name.") @RequestParam(value = "name", required = false) String name,
        @ApiParam(value = "Filter on the ICAO code.") @RequestParam(value = "icaoCode", required = false) String icaoCode,
        @ApiParam(value = "Filter active/inactive airlines.") @RequestParam(value = "active", required = false) Boolean active ,
        @ApiParam(value = "Filter on the country.") @RequestParam(value = "country", required = false) String country
) {
        
    	// Initialization 
    	List<Airline> airlines 	= new ArrayList<Airline>();
    	HttpStatus status 		= HttpStatus.NO_CONTENT; 
		
		 
		/** Note: For now this is very basic filtering without
		 * the AND capability. To be continued.
		 */
		if(id!=null && id > 0)
		{
			airlines = airlinesRepo.findById(id);
		}
		else if(icaoCode!=null && !icaoCode.isEmpty())
		{
			airlines = airlinesRepo.findByIcaoCode(icaoCode);
		}
		else if(name!=null && !name.isEmpty())
		{
			airlines = airlinesRepo.findByName(name);
		}
		else if(country!=null && !country.isEmpty())
		{
			airlines = airlinesRepo.findByCountry(country);
		}
		else if(active!=null)
		{
			airlines = airlinesRepo.findByActive(active);
		}
		else
		{
			airlines = (List<Airline>) airlinesRepo.findAll(); 
		}
		status = HttpStatus.OK;
     
    	
        return new ResponseEntity<List<Airline>>(airlines,status);
    }

	/***************************************************************
	 * FALLBACK METHODS
	 ***************************************************************/
    public ResponseEntity<List<Airline>> fallbackGetAirlines(Long id, String name, String icaoCode, Boolean active, String country)
    {
    	logger.warn("FALLBACK METHOD CALLED" );
    	List<Airline> airlinesEmpty = new ArrayList<Airline>(); 
    	return new ResponseEntity<List<Airline>>(airlinesEmpty,HttpStatus.OK); 
    }
}
