package io.swagger.api;

import io.swagger.model.Airline;
import io.swagger.model.ErrorResponse;
import io.swagger.model.IAirlinesRepository;
import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-28T23:18:49.392Z")

@Controller  
public class AirlineApiController implements AirlineApi {

	/***************************************************************
	 * MEMBER DATA 
	 ***************************************************************/
	@Autowired
    private IAirlinesRepository airlinesRepo;

	private static Logger logger = LoggerFactory.getLogger(AirlinesApiController.class);
	

	/***************************************************************
	 * API FUNCTIONS
	 ***************************************************************/
	
    public ResponseEntity<Airline> deleteAirline(
@ApiParam(value = "ID of the airline to delete",required=true ) @PathVariable("id") Long id


) { 
		Airline airline = airlinesRepo.findOne(id); 
		airlinesRepo.delete(airline);
		return new ResponseEntity<Airline>(airline, HttpStatus.OK); 
    }

    public ResponseEntity<Airline> getAirline(
@ApiParam(value = "ID of the airline in the system.",required=true ) @PathVariable("id") Long id


) {
        Airline airline = airlinesRepo.findOne(id);
        return new ResponseEntity<Airline>(airline, HttpStatus.OK);
    }

    public ResponseEntity<Airline> updateAirline(
@ApiParam(value = "ID of the airline to update",required=true ) @PathVariable("id") Long id


,
        

@ApiParam(value = "Updated airline object" ,required=true ) @RequestBody Airline airline

) {
        Airline oldAirline = airlinesRepo.findOne(id);
        
        if(airline.getActive()!=null) 
        	oldAirline.setActive(airline.getActive());
        if(airline.getCountry()!= null && !airline.getCountry().isEmpty())
        	oldAirline.setCountry(airline.getCountry());
        if(airline.getName()!= null && !airline.getName().isEmpty())
        	oldAirline.setName(airline.getName());
        if(airline.getIcaoCode()!= null && !airline.getIcaoCode().isEmpty())
        	oldAirline.setIcaoCode(airline.getIcaoCode());
        if(airline.getWebsite()!= null && !airline.getWebsite().isEmpty())
        	oldAirline.setWebsite(airline.getWebsite());
        
        Airline updatedAirline = airlinesRepo.save(oldAirline);
        
        return new ResponseEntity<Airline>(updatedAirline, HttpStatus.OK);
    }

}
