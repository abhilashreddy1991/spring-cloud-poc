package io.swagger.api;

import io.swagger.model.Airline;
import io.swagger.model.ErrorResponse;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-28T23:18:49.392Z")

@Api(value = "airline", description = "the airline API")
public interface AirlineApi {

    @ApiOperation(value = "", notes = "Deletes a single airline based on the specified ID", response = Airline.class, tags={ "airlines", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "Deleted airline's information", response = Airline.class),
        @ApiResponse(code = 200, message = "unexpected error", response = Airline.class) })
    @RequestMapping(value = "/airline/{id}",
        produces = { "application/json" },  
        method = RequestMethod.DELETE)
    ResponseEntity<Airline> deleteAirline(
@ApiParam(value = "ID of the airline to delete",required=true ) @PathVariable("id") Long id


);


    @ApiOperation(value = "", notes = "Returns the airline of the specified ID.", response = Airline.class, tags={ "airlines", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Found airline", response = Airline.class),
        @ApiResponse(code = 200, message = "Error", response = Airline.class) })
    @RequestMapping(value = "/airline/{id}",
        produces = { "application/json" },  
        method = RequestMethod.GET)
    ResponseEntity<Airline> getAirline(
@ApiParam(value = "ID of the airline in the system.",required=true ) @PathVariable("id") Long id


);


    @ApiOperation(value = "", notes = "Update the airline specified by the ID parameter.", response = Airline.class, tags={ "airlines", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Updated airline's information", response = Airline.class),
        @ApiResponse(code = 200, message = "unexpected error", response = Airline.class) })
    @RequestMapping(value = "/airline/{id}",
        produces = { "application/json" },  
        method = RequestMethod.PUT)
    ResponseEntity<Airline> updateAirline(
@ApiParam(value = "ID of the airline to update",required=true ) @PathVariable("id") Long id


,

@ApiParam(value = "Updated airline object" ,required=true ) @RequestBody Airline airline

);

}
