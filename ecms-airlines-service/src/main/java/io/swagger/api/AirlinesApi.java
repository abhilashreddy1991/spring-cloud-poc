package io.swagger.api;

import io.swagger.model.Airline;
import io.swagger.model.ErrorResponse;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile; 

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-28T23:18:49.392Z")

@Api(value = "airlines", description = "the airlines API")
public interface AirlinesApi {

	
	
	/********************************************** 
	 * CREATE AIRLINE  
	 *********************************************/
    @ApiOperation(value = "", notes = "Add a new airline to eCMS.", response = Airline.class, tags={ "airlines", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The newly added airline.", response = Airline.class),
        @ApiResponse(code = 200, message = "Error", response = Airline.class) })
    @RequestMapping(value = "/airlines",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Airline> createAirline(

@ApiParam(value = "New airline to add to the portal" ,required=true ) @RequestBody Airline airline

);


	/********************************************** 
	 * GET ALL AIRLINES
	 *********************************************/
    @ApiOperation(value = "", notes = "Returns the list of existing eCMS airlines to the caller.", response = Airline.class, responseContainer = "List", tags={ "airlines", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A list of airlines.", response = Airline.class),
        @ApiResponse(code = 200, message = "Error", response = Airline.class) })
    @RequestMapping(value = "/airlines",
        produces = { "application/json" },  
        method = RequestMethod.GET)
    ResponseEntity<List<Airline>> getAirlines(@ApiParam(value = "Filter on the airline ID ( equivalent to GET /airline/{id} )") @RequestParam(value = "id", required = false) Long id



,@ApiParam(value = "Filter on the airline's name.") @RequestParam(value = "name", required = false) String name



,@ApiParam(value = "Filter on the ICAO code.") @RequestParam(value = "icaoCode", required = false) String icaoCode



,@ApiParam(value = "Filter active/inactive airlines.") @RequestParam(value = "active", required = false) Boolean active



,@ApiParam(value = "Filter on the country.") @RequestParam(value = "country", required = false) String country



);
    
}
