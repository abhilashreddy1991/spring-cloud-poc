DROP DATABASE IF EXISTS `ecms-airlines-service` ;

CREATE DATABASE `ecms-airlines-service`;

DROP USER IF EXISTS `ecms-airlines-service`;
CREATE USER `ecms-airlines-service`@`%` IDENTIFIED BY 'a:8,V~/a@sDCg{9+';

GRANT ALL PRIVILEGES ON `ecms-airlines-service`.* TO `ecms-airlines-service`@`%`  ;

